use cairo;
use librsvg;

fn main() {
    let input = "org.gnome.TransparencyReproducer.svg";
    let width = 128.0;
    let height = 128.0;
    let output = "output.svg";

    let handle = librsvg::Loader::new().read_path(input).unwrap();
    let renderer = librsvg::CairoRenderer::new(&handle);

    let mut surface = cairo::SvgSurface::new(width, height, output);
    surface.set_document_unit (cairo::SvgUnit::Px);
    let cr = cairo::Context::new(&surface);

    let doc_width = renderer.intrinsic_dimensions ().width.unwrap ().length;
    let doc_height = renderer.intrinsic_dimensions ().height.unwrap ().length;
    let viewport = cairo::Rectangle {
        x: 0.0,	
        y: 0.0,
        width: doc_width,
        height: doc_height,
    };

    if let Ok((_, size)) = renderer.geometry_for_layer (Some("#hicolor"), &viewport) {
        renderer.render_document(&cr,
                                 &cairo::Rectangle {
                                     x: -size.x,
                                     y: -size.y,
                                     width: doc_width,
                                     height: doc_height,
                                 }
        ).unwrap ();
    }
}
